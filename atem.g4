/*
 Antlr grammar for the AGES Templates (atem)
 ? = zero or once
 * = zero or more
 */
grammar Atem;

atemModel:
	'Template'  qualifiedName 
	templateStatus?
	importBlock*
	head?
	preface?
	abstractComponent*
	'End-Template';

head: 'Head' headComponent+ 'End_Head'
;
headComponent:
	commemoration
	| setDate
	| mcDay
	| pageHeaderEven
	| pageHeaderOdd
	| pageFooterEven
	| pageFooterOdd
	| pageKeepWithNext
	| pageNumber
	| templateTitle
;
commemoration: 'Template_Commemoration' headerFooterFragment+ 'End_Template_Commemoration';

preface: 'Preface' name=ID prefaceElementType* 'End-Preface';
prefaceElementType: 
	  versionSwitch
	| block
	| paragraph
	| section 
	| sectionFragment 
	| subTitle
	| templateFragment
	|  title 
;
headerFooterFragment: 
	headerFooterText 
	| headerFooterDate 
	| headerFooterPageNumber 
	| headerFooterLookup
	| headerFooterTitle
	| headerFooterCommemoration
;
headerFooterText: '@text' STRING;
headerFooterDate: '@date'  ('lang' language)? ;
headerFooterPageNumber: '@pageNbr';
headerFooterLookup: '@lookup' elementType* 'lang' language;
headerFooterTitle: '@title';
headerFooterCommemoration: '@commemoration';
setDate: 'Set_Date' 'month' INT  'day' INT  ('year' INT)? 'End_Date';
mcDay: 'Set_mcDay' 'day' INT 'End_mcDay';

templateTitle: 'Template_Title' headerFooterFragment+ 'End_Title';
pageKeepWithNext: 'Page_Keep_With_Next' STRING 'End_Page_Keep_With_Next';
pageHeaderEven: 'Page_Header_Even' headerFooterColumn+ 'End_Page_Header_Even';
pageHeaderOdd: 'Page_Header_Odd'  headerFooterColumn+ 'End_Page_Header_Odd';
pageFooterEven: 'Page_Footer_Even'  headerFooterColumn+ 'End_Page_Footer_Even';
pageFooterOdd: 'Page_Footer_Odd'  headerFooterColumn+ 'End_Page_Footer_Odd';

headerFooterColumn: headerFooterColumnLeft | headerFooterColumnCenter | headerFooterColumnRight;
headerFooterColumnLeft: 'left' headerFooterFragment+;
headerFooterColumnCenter: 'center' headerFooterFragment+ ;
headerFooterColumnRight: 'right' headerFooterFragment+ ;

pageNumber: 'Set_Page_Number' INT 'End_Set_Page_Number';

abstractComponent: 
	actor
	| block
	| dialog
	| hymn
	| insertBreak
	| mcDay
	| media
	| paragraph
	| passThroughHtml
	| reading
	| restoreLocale
	| rubric
	| section
	| sectionFragment
	| setDate
	| setLocale
	| subTitle
	| templateFragment
	| title
	| verse 
	| version
	| versionSwitch
	| whenDate
	| whenMovableCycleDay
	| whenDayName
	| whenExists
	| whenLukanCycleDay
	| whenModeOfWeek
	| whenSundayAfterElevationOfCrossDay
	| whenSundaysBeforeTriodion
	| EOF
;
actor: 'Actor' elementType* 'End-Actor';
block: 'bTag' qualifiedName elementType* 'End-bTag';
dialog: 'Dialog' elementType* 'End-Dialog';
hymn: 'Hymn' elementType* 'End-Hymn';
media: 'Media' elementType* 'End-Media';
paragraph: 'Para' role? elementType* 'End-Para';
passThroughHtml: 'Passthrough-Html' STRING 'END-Passthrough-Html';
reading: 'Reading' elementType* 'End-Reading';
restoreLocale: '@restoreLocale';
rubric: 'Rubric' elementType* 'End-Rubric';
section: 'Section' ID ('role' qualifiedName)? abstractComponent* 'End-Section';
setLocale: 'Set_Locale' 'locale_1' STRING  'locale_2' STRING 'End_Set_Locale';
subTitle: 'Sub-Title' ('role' qualifiedName)? elementType* 'End-Sub-Title';
title: 'Title' role? elementType* 'End-Title';
verse: 'Verse' elementType* 'End-Verse';
version: 'Version' qualifiedName;
versionSwitch: 'Switch-Version' versionSwitchType 'End-Switch-Version';
versionSwitchType: 'L1' | 'L2' | 'Both';
whenDate:
    'when-date-is'
      whenDateCase+
      whenOther?
       'end-when'
;
whenDateCase:   
      monthName abstractDateCase 'use:'
      abstractComponent*
;
whenOther: 'otherwise use:' abstractComponent*;
whenPeriodCase:   
      abstractDayCase 'use:' abstractComponent*
;
abstractDayCase: dayRange | daySet;
dayRange: INT 'thru' INT;
daySet: INT ( ',' INT)*;
whenMovableCycleDay:
'when-movable-cycle-day-is'
whenPeriodCase+
whenOther?
'end-when'
;
abstractDateCase: dateRange | dateSet;
dateRange: INT 'thru' INT;
dateSet: INT (',' INT)*;

whenDayName:
       'when-name-of-day-is'
     whenDayNameCase+
      whenOther?
       'end-when'
;


whenDayNameCase:   
      abstractDayNameCase 'use:'
      abstractComponent*
;
abstractDayNameCase: dayNameRange | dayNameSet;
dayNameRange: dayOfWeek 'thru' dayOfWeek;
dayNameSet: dayOfWeek ( ',' dayOfWeek)*;

whenExists:
	'when-exists'
	whenExistsCase+
	whenOther?
	'end-when'
;
whenExistsCase:
      'rid' qualifiedName 'use:'
      abstractComponent*
;
whenModeOfWeek:
'when-mode-of-week-is'
whenModeOfWeekCase+
whenOther?
'end-when'
;
whenModeOfWeekCase:   
      modeOfWeekSet 'use:'
      abstractComponent*
;
modeOfWeekSet: modeTypes ( ',' modeTypes)*;

whenLukanCycleDay:
'when-Lukan-Cycle-Day-is'
whenPeriodCase+
whenOther?
'end-when'
;
whenSundayAfterElevationOfCrossDay:
   'when-Sunday-after-Elevation-of-Cross-is'
  whenDateCase+
  whenOther?
   'end-when'
;
whenSundaysBeforeTriodion:
'when-sundays-before-triodion-is'
sundaysBeforeTriodionCase+
whenOther?
'end-when'
;
sundaysBeforeTriodionCase:   
      INT 'use:'
      abstractComponent*
;
dayOverride: '@day' DOW;
elementType: resourceText | taggedText | lookup | ldp;
resourceText: 'sid' qualifiedName VER? MEDIAOFF?;
taggedText: '<' qualifiedName '>' elementType* '</>';
lookup: 'rid'qualifiedName VER? MEDIAOFF? modeOverride? dayOverride?;
ldp: 'ldp' ldpType*;
insertBreak: 'Break' breakType 'End_Break';
importBlock: 'import' qualifiedNameWithWildCard;
modeOverride: '@mode' modeTypes;
role: 'role' qualifiedName;
sectionFragment: 'Insert_section' qualifiedName 'End-Insert';
templateFragment: 'Insert_template' qualifiedName 'End-Insert';
templateStatus: 'Status' templateStatuses;
qualifiedName: ID('.'ID)*;
qualifiedNameWithWildCard: qualifiedName '.*'?;
VER: '@ver';
MEDIAOFF: 'media-off';
ldpType: DOM | DOWN | DOWT| EOW | All 
	| GenDate | GenYear | MCD | MOW | NOP | DOP | SAEC | SOL | DOL
	| WOLC | WDOLC |SBT;
All: '@All_Liturgical_Day_Properties';
DOL: '@Lukan_Cycle_Elapsed_Days';
DOM: '@Day_of_Month'; 
DOP: '@Day_of_Period'; 
DOW: 'D1' | 'D2' | 'D3' | 'D4' | 'D5' | 'D6' | 'D7';
DOWN: '@Day_of_Week_As_Number';  
DOWT: '@Day_of_Week_As_Text';
EOW: '@Eothinon';
GenDate: '@Service_Date'; 
GenYear: '@Service_Year'; 
MCD: '@Day_of_Movable_Cycle'; 
MOW: '@Mode_of_Week'; 
NOP: '@Name_of_Period'; 
SAEC: '@Sunday_After_Elevation_Cross_Date';
SBT: '@Sundays_Before_Triodion';	
SOL: '@Lukan_Cycle_Start_Date';
WDOLC: '@Lukan_Cycle_Week_Day';
WOLC: '@Lukan_Cycle_Week';

ID: ([a-z]|[A-Z]|'_')([a-z]|[A-Z]|[0-9]|'_')*;

// enums
breakType: 'line' | 'page';
language: 'L1' | 'L2';
dayOfWeek: 'Sunday' | 'Monday' | 'Tuesday' | 'Wednesday' | 'Thursday' | 'Friday' | 'Saturday';
modeTypes: 'M1' | 'M2' | 'M3' | 'M4' | 'M5' | 'M6' | 'M7' | 'M8';
monthName: 'Jan' | 'Feb' | 'Mar' | 'Apr' | 'May' | 'Jun' | 'Jul' | 'Aug' | 'Sep' | 'Oct' | 'Nov' | 'Dec';
templateStatuses: 'NA' | 'Draft' | 'Review' | 'Final';
//VersionSwitchType: Language | 'Both';

STRING : '"' (ESC|.)*? '"' ;
ESC: '\\"' | '\\\\';
INT: [0-9][0-9]*;

// hidden from parser, but available in generated code
COMMENT:
	'/*' .*? '*/' -> channel(HIDDEN) ; // match anything between /* and */
LINE_COMMENT: '//' ~[\r\n]* '\r'? '\n' -> channel(HIDDEN);
EOL: [\r\t\u000C\n]+ -> channel(HIDDEN);
// skips
WS: [ ]+ -> skip;
UNKNOWN_CHAR: .;