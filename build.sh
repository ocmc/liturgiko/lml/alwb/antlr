#!/bin/bash
rm -R ./golang/parser
rm -R ./java
antlr -o ./java -visitor Atem.g4
cd java
javac -cp /usr/local/Cellar/antlr/4.11.1/antlr-4.11.1-complete.jar *.java
cd ..
antlr -o ./golang/parser -visitor -Dlanguage=Go Atem.g4

