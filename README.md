# alwb-antlr

This project defines an ANLTR version of the ALWB template DSL with a generated golang listener.  The listener is used to convert ALWB templates to Doxa templates, which use the Liturgical Markup Language (LML).
